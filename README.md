
An example of building a simple CPU feature detection tool by reusing
`base::CPU` class from the Chromium project.

Usage:

    make
    ./cpu-detect

Example output:

    Vendor: GenuineIntel
    Brand: Intel(R) Core(TM) i7-2677M CPU @ 1.80GHz
    CPU features:
     MMX SSE SSE2 SSE3 SSSE3 SSE41 SSE42 AXV

Note that this only works on x86 platforms on Unix.
