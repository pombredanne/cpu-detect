CC=g++
CFLAGS=-c -Wall -Wa,-q -I. -Ibase/
LDFLAGS=-I. -Ibase -Wa,-q
SOURCES=cpu-detect.cc base/cpu.cc
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=cpu-detect

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

.PHONY: clean

clean:
	rm -f *.o a.out *~ core $(EXECUTABLE)
