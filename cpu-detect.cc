#include <iostream>
#include "base/cpu.h"

using base::CPU;

int main(int argc, char** argv) {
    CPU cpu;
    std::cout << "Vendor: " << cpu.vendor_name() << std::endl;
    std::string brand = cpu.cpu_brand();
    brand.erase(0, brand.find_first_not_of(' '));
    std::cout << "Brand: " << brand << std::endl;
    std::cout << "CPU features:" << std::endl;
    if (cpu.has_mmx()) std::cout << " MMX";
    if (cpu.has_sse()) std::cout << " SSE";
    if (cpu.has_sse2()) std::cout << " SSE2";
    if (cpu.has_sse3()) std::cout << " SSE3";
    if (cpu.has_ssse3()) std::cout << " SSSE3";
    if (cpu.has_sse41()) std::cout << " SSE41";
    if (cpu.has_sse42()) std::cout << " SSE42";
    if (cpu.has_avx()) std::cout << " AXV";
    std::cout << std::endl;
    return 0;
}
